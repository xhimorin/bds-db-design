
--
-- Data for Name: awards; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.awards (id_awards, title, description) VALUES (1, 'Oscar', 'Za najlepší film.');
INSERT INTO public.awards (id_awards, title, description) VALUES (2, 'Oscar', 'Za najlepší filmovou hudbu.');
INSERT INTO public.awards (id_awards, title, description) VALUES (3, 'Oscar', 'Za najlepší kameru.');
INSERT INTO public.awards (id_awards, title, description) VALUES (4, 'Oscar', 'Za najlepší střih zvuku.');
INSERT INTO public.awards (id_awards, title, description) VALUES (5, 'MTV Movie Award', 'Za najlepší souboj.');


--
-- Data for Name: genders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.genders (id_genders, gender_type) VALUES (1, 'muž');
INSERT INTO public.genders (id_genders, gender_type) VALUES (2, 'žena');
INSERT INTO public.genders (id_genders, gender_type) VALUES (3, 'x-men');


--
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.genres (id_genres, genre_type) VALUES (1, 'Sci-Fi');
INSERT INTO public.genres (id_genres, genre_type) VALUES (2, 'Akční');
INSERT INTO public.genres (id_genres, genre_type) VALUES (3, 'Dobrodružný');
INSERT INTO public.genres (id_genres, genre_type) VALUES (4, 'Drama');
INSERT INTO public.genres (id_genres, genre_type) VALUES (5, 'Krimi');
INSERT INTO public.genres (id_genres, genre_type) VALUES (6, 'Thriller');
INSERT INTO public.genres (id_genres, genre_type) VALUES (7, 'Komedie');
INSERT INTO public.genres (id_genres, genre_type) VALUES (8, 'Rodinný');
INSERT INTO public.genres (id_genres, genre_type) VALUES (9, 'Romantický');
INSERT INTO public.genres (id_genres, genre_type) VALUES (10, 'Mysteriózní');
INSERT INTO public.genres (id_genres, genre_type) VALUES (11, 'Psychologický');
INSERT INTO public.genres (id_genres, genre_type) VALUES (12, 'Horor');
INSERT INTO public.genres (id_genres, genre_type) VALUES (13, 'Sportovní');
INSERT INTO public.genres (id_genres, genre_type) VALUES (14, 'Životopisný');


--
-- Data for Name: interests; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.interests (id_interests, interest) VALUES (1, 'Režisér Denis Villeneuve preferoval při natáčení využívání praktických efektů, proto natáčení probíhalo převážně v exteriérech. „Čelisti se také nenatáčely v bazénu,“ vysvětlil rozhodnutí Villeneuve.');
INSERT INTO public.interests (id_interests, interest) VALUES (2, 'Závěrečná scéna byla ve skutečnosti natočena na Panenských ostrovech v Karibiku, avšak ve filmu se má odehrávat na pobřeží Tichého oceánu.');
INSERT INTO public.interests (id_interests, interest) VALUES (3, 'Hned při první scéně, kde má hlavní postava (Edward Norton) na parkovišti udeřit Tylera (Brad Pitt) "silně, jak jen může", Norton opravdu trefí Pitta do ucha, ač měl úder jen předstírat. To proto, že si režisér David Fincher před scénou vzal Nortona stranou a řekl mu, ať Pitta udeří doopravdy, aby záběr vypadal zcela reálně. Proto ve výsledné scéně můžeme vidět, jak je Pitt skutečně v bolestech a Norton se jen směje.');


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (1, 'Duna', 2021, 'USA', 'https://www.youtube.com/watch?v=8g18jFHCLXk', 'Kultovní sci-fi dílo vypráví o mocenských bojích uvnitř galaktického Impéria, v nichž jde o ovládnutí planety Arrakis: zdroje vzácného koření - melanže, jež poskytuje zvláštní psychické schopnosti, které umožňují cestování vesmírem. Padišáh imperátor svěří správu nad Arrakisem a s ní i komplikovanou těžbu neobyčejné látky vévodovi rodu Atreidů. Celá anabáze je ale součástí spiknutí, z něhož se podaří vyváznout jen vévodovu synovi Paulovi a jeho matce, kteří uprchnou do pouště. Jejich jedinou nadějí jsou nedůvěřiví domorodí obyvatelé fremeni, schopní trvale přežít ve vyprahlé pustině. Mohl by Paul Atreides být spasitelem, na kterého tak dlouho čekají?');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (2, 'Vykoupení z věznice Shawshank', 1994, 'USA', 'https://www.youtube.com/watch?v=iorHgfCOhK8', 'Mladý bankovní manažer Andy Dufresne (Tim Robbins) je v roce 1947 odsouzen na doživotí za vraždu své ženy a jejího milence, kterou nespáchal. Čeká ho trest v obávané věznici Shawshank. Andy se zde sblíží s černochem Redem (Morgan Freeman), jenž je tu už dvacet let, a během dlouhé doby se jim společně podaří dosáhnout zlepšení zdejších poměrů. Andy se dokonce stane strážcům i nenáviděnému řediteli věznice nepostradatelný jako daňový a finanční poradce.');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (3, 'Klub rváčů', 1999, 'USA', 'https://www.youtube.com/watch?v=MAidmBZD0Bk', 'Když nemůžete půl roku usnout, celý okolní svět vám začne připadat jako nekonečný sen. Všechno kolem vás je nedokonalou xeroxovou kopií sebe sama. Chodíte do práce, díváte se na televizi a jste vděčni za to, když občas ztratíte vědomí a nevíte o světě. Lidí s podobnými problémy moc není, ale mladý úspěšný úředník, který si říká Jack, je jedním z nich. Má slušnou práci, vydělává slušné peníze, ale trpí nejtěžší formou nespavosti. Na služební cestě se Jack seznámí s Tylerem Durdenem, který mu nabídne příbytek pod podmínkou, že mu vrazí pořádnou ránu. Tato "výměna názorů" se oběma zalíbí a brzy vznikne první Klub rváčů. Místo, kde můžou mladí muži, znechucení světem, odložit své starosti a stát se na pár minut zvířaty.');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (4, 'Hon', 2012, 'Dánsko', 'https://www.youtube.com/watch?v=MxQi3KUXc94', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (5, 'Na samotě u lesa', 1976, 'Československo', NULL, 'Úsměvný příběh, ve kterém možná poznáte sami sebe. Pátečním odpolednem vyjíždí Oldřich a Věra Lavičkovi se svými dětmi poprvé na chalupu. Sice žádnou nemají, ale touží po ní a doufají, že přátelé v Loukově jim již nějakou vyhlédli. A opravdu. Jenomže v ní bydlí děda Komárek. Jak to bývalo zvykem, slíbí mu, že ho nechají v chalupě dožít, ale soukromí by přece jenom bylo lepší. Jedině děti přilnou k dědovi, jako by byl jejich vlastní, a jdou tak rodičům příkladem. Když děda onemocní, Lavičkovi si uvědomí, že nemají žádný doklad o tom, že jim chalupa bude po dědově smrti patřit, a bleskově se vypraví i s notářem do Loukova...');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (6, 'Nedotknutelní', 2011, 'Francie', 'https://www.youtube.com/watch?v=d1hTDI4lt0k', 'Ochrnutý a bohatý aristokrat Philippe si za svého nového opatrovníka vybere Drisse, živelného mladíka z předměstí, kterého právě propustili z vězení. Jinými slovy - najde si na tuto práci tu nejméně vhodnou osobu. Podaří se jim však propojit nemožné: Vivaldiho a populární hudbu, serióznost a žoviální vtípky, luxusní obleky a tepláky. Bláznivým, zábavným, silným, neočekávaným a hlavně „nedotknutelným“, přesně takovým se stane jejich přátelství… Komedie s dramatickou zápletkou o tom, že ani od krku po prsty u nohou nepohyblivý člověk odkázaný na pomoc druhých, nemusí ztratit smysl života. A o tom, že i nejméně pravděpodobné spojení melancholického multimilionáře a extrovertního recidivisty může humorně zapůsobit na diváka a může se z něj stát kasovní trhák.');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (7, 'Pulp Fiction: Historky z podsvětí', 1994, 'USA', 'https://www.youtube.com/watch?v=zt1uSDXL5NA', 'Scenárista, režisér a herec Quentin Tarantino (nar. 1963) je bezesporu jedním z nejvýznamnějších tvůrců současného světového filmu. Zaujal už svojí celovečerní prvotinou Gauneři (1991) a svým druhým filmem Pulp Fiction – historky z podsvětí (1994) jen potvrdil své dominantní postavení mezi postmoderními režiséry. Scénář filmu vznikl důmyslným propojením tří povídek spjatých se světem zločinu, jež jsou vyprávěny bez ohledu na časovou posloupnost a nakonec vytvoří uzavřený kruh. Původní název filmu, odkazující k pokleslé krvákové literatuře, dokonale souzní s tím, co se děje na plátně. Výbuchy brutálního násilí jsou odlehčeny velmi černým humorem a jedna bizarní situace za druhou ústí do ještě bizarnějších konců. Kromě novátorského scénáře a jisté režie film zaujme i přehlídkou mimořádných hereckých výkonů přesně obsazených představitelů, mj. Johna Travolty a Samuela L. Jacksona (zabijáci Vincent a Jules), Bruce Willise (unavený boxer Butch), Tima Rotha a Amandy Plummerové (milenecká dvojice, chystající se k přepadení) či Umy Thurmanové (gangsterská manželka Mia). Snímek byl uveden v soutěži MFF v Cannes 1994, odkud si odnesl nejvyšší ocenění, Zlatou palmu. Členové Americké akademie ho nominovali na Oscara v sedmi kategoriích (mj. i za nejlepší film a režii), ale zlatou sošku nakonec dostal pouze scénář filmu. V témže roce dominoval v rámci nezávislé tvorby a získal celkem čtyři Independent Spirit Awards (Ceny nezávislého ducha), a sice za nejlepší film, režii, scénář a herecký výkon v hlavní mužské roli (Samuel L. Jackson).');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (8, 'Gran Torino', 2008, 'Německo', 'https://www.youtube.com/watch?v=RMhbr2XQblk', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (9, 'Zelená míle', 1999, 'USA', 'https://www.youtube.com/watch?v=Ki4haFrqSrw', 'Brilantní filmové drama Zelená míle podle knižní předlohy Stephena Kinga natočil režisér Frank Darabont, který se zároveň podílel i na jeho scénáři. Film s vynikajícím Tomem Hanksem v hlavní roli, byl nominován na čtyři Oscary. Centrem příběhu je černý obr John Coffey (Michael Clarke Duncan), čekající na popravu za zločin, který dost možná nespáchal a přesto zemře, a dále pak věčné dilema kolem rozhodování o trestu smrti. Děj filmu se odehrává ve věznici, kde sedí ti největší hříšníci, a kde je hrůzostrašná chodba natřená na zeleno, které všichni říkají Zelená míle. Chodba, na jejímž konci čeká smrt. Pokud se nestane zázrak...');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (10, 'Forrest Gump', 1994, 'USA', 'https://www.youtube.com/watch?v=bLvqoHBptjg', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (11, 'Kmotr', 1972, 'USA', NULL, 'Doposud nevinný syn mafiánského bosse je vtažen do krvavého rodinného byznysu, když je jeho otec, Don Vito Corleone, hlava newyorské mafiánské rodiny vážně zraněn. Don Vito Corleone je hlava newyorské mafiánské „rodiny“. Když gangster Sollozzo, který má za zády jinou rodinu mafiánů, oznámí, že chce po celém New Yorku prodávat drogy, přichází problém. Don Vito se důrazně staví proti obchodu s drogami a docela mu stačí hazard, vybírání výpalného a podobné aktivity, kterými si vydělává. Dojde tedy k pokusu o jeho vraždu. Sollozzo pak unese jednoho z poradců Dona Vita a pokusí se přinutit syna Dona Vita, aby s prodejem drog souhlasil, plán ovšem zkrachuje, když Sollozzo zjistí, že je Don Vito stále naživu');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (12, 'Mlčení jehňátek', 1991, 'USA', 'https://www.youtube.com/watch?v=lu-pBQ7kNb4', 'Mladá citlivá agentka FBI Clarice Steriling je přizvána ke spolupráci do týmu vyšetřujícího sérii vražd. Na pachatele ukazují jen velmi kruté následky jeho činů: Vrah přezdívaný "Buffalo Bill" stahuje vždycky své oběti z kůže a potom je hází do řeky. Clarice má za úkol vypracovat psychologický portrét pachatele. Její nadřízení ji proto vyšlou na "konzultaci" za jiným masovým vrahem, psychiatrem dr. Hannibalem Lecterem, který je vězněn za nejvyšších bezpečnostních opatření. Lecter, který zná ze své praxe vraha i jeho motivy a tuší jeho další postup, drží v rukou všechny nitky a přivádí mladou Clarice na správnou stopu...');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (13, 'Sedm', 1995, 'USA', 'https://www.youtube.com/watch?v=znmZoVkCjpI', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (14, 'The Matrix', 1999, 'USA', 'https://www.youtube.com/watch?v=vKQi3bBA1y8', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (15, 'Vetřelec', 1979, 'Velká Británie', NULL, 'Vesmírná loď Nostromo míří zpět na Zemi, astronauti spí. Náhle dojde k jejich automatickému probuzení – nikdo neví, co se děje, protože jsou ještě velmi daleko od cíle cesty. Prostřednictvím centrálního počítače zjistí, že přijímače zachytily signál SOS z blízké planety. Posádka je povinna věc vyšetřit. Vyslaný modul má s přistáním velké problémy. Přesto někteří na průzkum. V jeskynním komplexu objeví podivný organizmus, který vypadá jako kolonie vajíček. Počítač mateřské lodi mezitím dešifruje zprávu jako varování, ne jako volání o pomoc. Všichni jsou okamžitě odvoláni. Jenže jedno z vajíček se otevře a jakási hmota ve tvaru chobotnice se během setiny sekundy přisaje na přilbu jednoho z astronautů, přesněji řečeno na jeho obličej. Přes odpor Ripleyové, že je proti bezpečnostním předpisům brát na palubu cokoli neznámého, otevře Ash dveře a všechny vpustí. Po nějaké době je nalezen cizí organismus na podlaze, sám odpadl. Vypadá to, že se postižený těší dobrému zdraví. Při společném jídle však stolující zažijí hrůzné představení. Před jejich očima vyleze z astronautova břicha nějaký tvor, a než stačí kdokoli cokoli podniknout, bleskovou rychlostí zmizí v obrovských prostorách lodi. Začíná noční můra – monstrum postupně likviduje posádku a zdá se, že je nezničitelné… ');
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (16, 'Rivalové', 2013, 'Německo', 'https://www.youtube.com/watch?v=6k0QyHapx0w', NULL);
INSERT INTO public.movies (id_movies, title, publication_year, country, url_trailer, contents) VALUES (17, 'Počátek', 2010, 'Velká Británie', 'https://www.youtube.com/watch?v=6XJ4JQFFYO8', 'Dom Cobb (Leonardo DiCaprio) je velmi zkušený zloděj a jeho největší mistrovství je v krádeži nejcennějších tajemství. Ovšem není to jen tak obyčejný zloděj. Dom krade myšlenky z lidského podvědomí v době, kdy lidská mysl je nejzranitelnější – když člověk spí. Cobbova nevšední dovednost z něj dělá nejen velmi vyhledávaného experta, ale také ohroženého uprchlíka. Musel obětovat vše, co kdy miloval. Nyní se mu však nabízí šance na vykoupení. Může získat zpět svůj život. Tato poslední zakázka je nejen velmi riskantní, ale zdá se, že i nemožná. Tentokrát nemá za úkol myšlenku ukrást, ale naopak ji zasadit do něčí mysli. Pokud uspěje, bude to dokonalý zločin.');


--
-- Data for Name: movies_has_awards; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies_has_awards (id_movies, id_awards, award_year) VALUES (2, 1, 1995);
INSERT INTO public.movies_has_awards (id_movies, id_awards, award_year) VALUES (2, 2, 1995);
INSERT INTO public.movies_has_awards (id_movies, id_awards, award_year) VALUES (2, 3, 1995);
INSERT INTO public.movies_has_awards (id_movies, id_awards, award_year) VALUES (3, 4, 2000);
INSERT INTO public.movies_has_awards (id_movies, id_awards, award_year) VALUES (3, 5, 2000);


--
-- Data for Name: movies_has_genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (1, 1);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (1, 2);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (1, 3);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (1, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (2, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (2, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (3, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (3, 6);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (4, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (5, 7);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (5, 8);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (5, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (6, 7);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (6, 14);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (6, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (7, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (7, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (8, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (9, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (9, 10);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (9, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (10, 7);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (10, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (10, 9);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (11, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (11, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (12, 6);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (12, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (12, 10);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (12, 11);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (12, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (13, 5);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (13, 6);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (13, 12);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (13, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (14, 1);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (14, 2);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (15, 1);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (15, 12);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (16, 2);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (16, 4);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (16, 13);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (16, 14);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (17, 1);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (17, 2);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (17, 3);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (17, 6);
INSERT INTO public.movies_has_genres (id_movies, id_genres) VALUES (17, 10);


--
-- Data for Name: movies_has_interests; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies_has_interests (id_movies, id_interests) VALUES (1, 1);
INSERT INTO public.movies_has_interests (id_movies, id_interests) VALUES (2, 2);
INSERT INTO public.movies_has_interests (id_movies, id_interests) VALUES (3, 3);


--
-- Data for Name: persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (1, 1, 'Denis', 'Villeneuve', '1967-10-03', 'Kanada');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (2, 2, 'Rebecca', 'Ferguson', '1983-10-19', NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (3, 1, 'Timothée', 'Chalamet', '1995-12-27', NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (4, 1, 'Frank', 'Darabont', '1959-01-28', 'Francie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (5, 1, 'Morgan', 'Freeman', '1937-06-01', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (6, 1, 'David', 'Fincher', '1962-08-28', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (7, 1, 'Edward', 'Norton', '1969-08-18', NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (8, 1, 'Brad', 'Bitt', NULL, NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (9, 1, 'Thomas', 'Vinterberg', '1969-05-19', 'Dánsko');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (10, 1, 'Mads', 'Mikkelsen', '1965-11-22', 'Dánsko');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (11, 2, 'Annika', 'Wedderkopp', NULL, NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (12, 1, 'Jiří', 'Menzel', '1938-02-05', 'Česko');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (13, 1, 'Josef', 'Kemr', '1922-06-15', 'Česko');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (14, 2, 'Naďa', 'Urbánková', NULL, NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (15, 1, 'Eric', 'Toledano', '1971-07-03', 'Francie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (16, 1, 'Omar', 'Sy', '1978-01-20', 'Francie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (17, 2, 'Joséphine', 'Meaux', '1977-01-23', 'Francie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (18, 1, 'Quentin', 'Tarantino', '1963-03-27', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (19, 1, 'John', 'Travolta', '1954-02-18', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (20, 1, 'Samuel', 'Jackson', '1948-12-21', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (21, 1, 'Clint', 'Eastwood', '1930-05-31', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (22, 2, 'Dreama', 'Walker', '1986-06-20', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (23, 2, 'Ahney', 'Her', '1992-07-13', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (24, 1, 'Tom', 'Hanks', '1956-07-09', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (25, 1, 'David', 'Morse', '1953-10-11', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (26, 1, 'Robert', 'Zemeckis', '1951-05-14', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (27, 2, 'Robin', 'Wright', '1966-04-08', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (28, 1, 'Francis', 'Coppola', '1939-04-07', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (29, 1, 'Marlon', 'Brando', '1924-04-03', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (30, 1, 'Al', 'Capone', '1940-04-25', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (31, 1, 'Jonathan', 'Demme', '1944-02-22', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (32, 2, 'Jadie', 'Foster', '1962-11-19', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (33, 1, 'Anthony ', 'Hopkins', '1937-12-31', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (34, 2, 'Lilly', 'Wachowski', '1967-12-29', NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (35, 1, 'Keanu', 'Reeves', '1964-09-02', 'Libanon');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (36, 1, 'Laurence', 'Fishburne', '1961-07-30', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (37, 1, 'Ridley', 'Scott', '1937-11-30', 'Velká Británie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (38, 2, 'Sigourney', 'Weaver', NULL, NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (39, 1, 'Tom', 'Skerritt', '1933-08-25', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (40, 2, 'Veronica', 'Cartwright', '1949-04-20', 'Velká Británie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (41, 1, 'Ron', 'Howard', '1954-03-01', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (42, 1, 'Daniel', 'Brühl', '1978-06-16', 'Španělsko');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (43, 1, 'Chris', 'Hemsworth', '1983-08-11', 'Austrálie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (44, 1, 'Christopher', 'Nolan', '1970-07-30', 'Velká Británie');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (45, 1, 'Leonardo', 'DiCaprio', '1974-11-11', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (46, 3, 'Elliot', 'Page', '1987-02-21', 'Kanada');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (47, 1, 'Miloš', 'Forman', '1932-02-18', 'Československo');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (48, 2, 'Louise', 'Fletcher', '1934-07-22', 'USA');
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (49, 1, 'Owen', 'Wilson', NULL, NULL);
INSERT INTO public.persons (id_persons, id_genders, first_name, surname, birthdate, country) VALUES (50, 1, 'Jack', 'Nicholson', '1937-04-22', NULL);

--
-- Data for Name: persons_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.persons_type (id_persons_type, person_type) VALUES (1, 'Režisér');
INSERT INTO public.persons_type (id_persons_type, person_type) VALUES (2, 'Herec');


--
-- Data for Name: movies_has_persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (1, 1, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (1, 2, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (1, 3, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (2, 4, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (2, 5, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (2, 6, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (3, 7, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (3, 8, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (3, 9, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (4, 10, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (4, 11, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (4, 12, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (5, 13, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (5, 14, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (5, 15, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (6, 16, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (6, 17, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (6, 18, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (7, 19, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (7, 20, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (7, 21, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (8, 22, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (8, 23, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (8, 24, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (9, 25, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (9, 26, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (9, 27, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (10, 28, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (10, 26, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (10, 29, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (11, 31, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (11, 32, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (11, 33, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (12, 34, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (12, 35, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (12, 36, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (13, 6, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (13, 5, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (13, 8, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (14, 37, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (14, 38, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (14, 39, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (15, 40, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (15, 41, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (15, 42, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (16, 43, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (17, 44, 1);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (17, 45, 2);
INSERT INTO public.movies_has_persons (id_movies, id_persons, id_persons_type) VALUES (17, 46, 2);


--
-- Data for Name: roles_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles_type (id_roles_type, role_type) VALUES (1, 'admin');
INSERT INTO public.roles_type (id_roles_type, role_type) VALUES (2, 'užívateľ');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (id_users, id_roles_type, id_genders, first_name, surname, mail, username, birthdate) VALUES (1, 1, 3, 'Admin', 'Adminovský', 'admin@admin.ad', 'admin', '1998-12-24');
INSERT INTO public.users (id_users, id_roles_type, id_genders, first_name, surname, mail, username, birthdate) VALUES (2, 2, 2, 'Maroš', 'German', 'grc@mail..', 'grg', '2000-01-01');
INSERT INTO public.users (id_users, id_roles_type, id_genders, first_name, surname, mail, username, birthdate) VALUES (3, 2, 1, 'Roman', 'Klampár', 'rk@kr.rrr', 'xkl', '1978-09-09');
INSERT INTO public.users (id_users, id_roles_type, id_genders, first_name, surname, mail, username, birthdate) VALUES (4, 2, 2, 'Ján', 'Špageta', 'jjj@sss.qq', 'recenzent', '2002-08-08');
INSERT INTO public.users (id_users, id_roles_type, id_genders, first_name, surname, mail, username, birthdate) VALUES (5, 2, 1, 'John', 'Doe', 'john@doe.cz', 'johan', '1970-02-12');


--
-- Data for Name: ratings; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (1, 2, 1, 4, 'Paradny film.');
INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (2, 2, 5, 4, 'Paradny film.');
INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (3, 3, 4, NULL, 'Radsej to nehodnotim.');
INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (4, 4, 4, 3, 'Celkom dobry film.');
INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (5, 4, 5, 5, 'Kraaaaaasa');
INSERT INTO public.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) VALUES (6, 4, 2, 5, NULL);


--
-- Data for Name: users_credentials; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users_credentials (id_users_credentials, id_users, user_password) VALUES (1, 1, 'admin');
INSERT INTO public.users_credentials (id_users_credentials, id_users, user_password) VALUES (2, 2, '574&Hhca2');
INSERT INTO public.users_credentials (id_users_credentials, id_users, user_password) VALUES (3, 3, '@#&!%sss9');
INSERT INTO public.users_credentials (id_users_credentials, id_users, user_password) VALUES (4, 4, 'heslo');
INSERT INTO public.users_credentials (id_users_credentials, id_users, user_password) VALUES (5, 5, 'toto_nieje@heslo');


