--
-- Name: awards; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.awards (
    id_awards integer NOT NULL,
    title character varying(32) NOT NULL,
    description character varying(256)
);


ALTER TABLE public.awards OWNER TO postgres;

--
-- Name: genders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genders (
    id_genders integer NOT NULL,
    gender_type character varying(32) NOT NULL
);


ALTER TABLE public.genders OWNER TO postgres;

--
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genres (
    id_genres integer NOT NULL,
    genre_type character varying(32) NOT NULL
);


ALTER TABLE public.genres OWNER TO postgres;

--
-- Name: interests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interests (
    id_interests integer NOT NULL,
    interest character varying(512) NOT NULL
);


ALTER TABLE public.interests OWNER TO postgres;

--
-- Name: movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies (
    id_movies bigint NOT NULL,
    title character varying(128) NOT NULL,
    publication_year integer NOT NULL,
    country character varying(32) NOT NULL,
    url_trailer character varying(256),
    contents character varying(2048)
);


ALTER TABLE public.movies OWNER TO postgres;

--
-- Name: movies_has_awards; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies_has_awards (
    id_movies bigint NOT NULL,
    id_awards integer NOT NULL,
    award_year integer NOT NULL
);


ALTER TABLE public.movies_has_awards OWNER TO postgres;

--
-- Name: movies_has_genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies_has_genres (
    id_movies bigint NOT NULL,
    id_genres integer NOT NULL
);


ALTER TABLE public.movies_has_genres OWNER TO postgres;

--
-- Name: movies_has_interests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies_has_interests (
    id_movies bigint NOT NULL,
    id_interests integer
);


ALTER TABLE public.movies_has_interests OWNER TO postgres;

--
-- Name: movies_has_persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies_has_persons (
    id_movies bigint NOT NULL,
    id_persons bigint NOT NULL,
    id_persons_type integer NOT NULL
);


ALTER TABLE public.movies_has_persons OWNER TO postgres;

--
-- Name: persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persons (
    id_persons bigint NOT NULL,
    id_genders integer NOT NULL,
    first_name character varying(64) NOT NULL,
    surname character varying(64) NOT NULL,
    birthdate date,
    country character varying(32)
);


ALTER TABLE public.persons OWNER TO postgres;

--
-- Name: persons_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persons_type (
    id_persons_type integer NOT NULL,
    person_type character varying(32) NOT NULL
);


ALTER TABLE public.persons_type OWNER TO postgres;

--
-- Name: ratings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ratings (
    id_ratings bigint NOT NULL,
    id_users bigint NOT NULL,
    id_movies bigint NOT NULL,
    rating_movie integer,
    comment_movie character varying(512)
);


ALTER TABLE public.ratings OWNER TO postgres;

--
-- Name: roles_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles_type (
    id_roles_type integer NOT NULL,
    role_type character varying(32) NOT NULL
);


ALTER TABLE public.roles_type OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id_users bigint NOT NULL,
    id_roles_type integer NOT NULL,
    id_genders integer NOT NULL,
    first_name character varying(64) NOT NULL,
    surname character varying(64) NOT NULL,
    mail character varying(64) NOT NULL,
    username character varying(64) NOT NULL,
    birthdate date NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_credentials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_credentials (
    id_users_credentials bigint NOT NULL,
    id_users bigint NOT NULL,
    user_password character varying(512) NOT NULL
);


ALTER TABLE public.users_credentials OWNER TO postgres;

--
-- Name: awards awards_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.awards
    ADD CONSTRAINT awards_pkey PRIMARY KEY (id_awards);


--
-- Name: genders genders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genders
    ADD CONSTRAINT genders_pkey PRIMARY KEY (id_genders);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id_genres);


--
-- Name: interests interests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interests
    ADD CONSTRAINT interests_pkey PRIMARY KEY (id_interests);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id_movies);


--
-- Name: persons persons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT persons_pkey PRIMARY KEY (id_persons);


--
-- Name: persons_type persons_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons_type
    ADD CONSTRAINT persons_type_pkey PRIMARY KEY (id_persons_type);


--
-- Name: ratings ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id_ratings);


--
-- Name: roles_type roles_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles_type
    ADD CONSTRAINT roles_type_pkey PRIMARY KEY (id_roles_type);


--
-- Name: users_credentials users_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_credentials
    ADD CONSTRAINT users_credentials_pkey PRIMARY KEY (id_users_credentials);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id_users);


--
-- Name: movies_has_awards movies_has_awards_id_awards_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_awards
    ADD CONSTRAINT movies_has_awards_id_awards_fkey FOREIGN KEY (id_awards) REFERENCES public.awards(id_awards);


--
-- Name: movies_has_awards movies_has_awards_id_movies_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_awards
    ADD CONSTRAINT movies_has_awards_id_movies_fkey FOREIGN KEY (id_movies) REFERENCES public.movies(id_movies);


--
-- Name: movies_has_genres movies_has_genres_id_genres_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_genres
    ADD CONSTRAINT movies_has_genres_id_genres_fkey FOREIGN KEY (id_genres) REFERENCES public.genres(id_genres);


--
-- Name: movies_has_genres movies_has_genres_id_movies_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_genres
    ADD CONSTRAINT movies_has_genres_id_movies_fkey FOREIGN KEY (id_movies) REFERENCES public.movies(id_movies);


--
-- Name: movies_has_interests movies_has_interests_id_interests_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_interests
    ADD CONSTRAINT movies_has_interests_id_interests_fkey FOREIGN KEY (id_interests) REFERENCES public.interests(id_interests);


--
-- Name: movies_has_interests movies_has_interests_id_movies_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_interests
    ADD CONSTRAINT movies_has_interests_id_movies_fkey FOREIGN KEY (id_movies) REFERENCES public.movies(id_movies);


--
-- Name: movies_has_persons movies_has_persons_id_movies_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_persons
    ADD CONSTRAINT movies_has_persons_id_movies_fkey FOREIGN KEY (id_movies) REFERENCES public.movies(id_movies);


--
-- Name: movies_has_persons movies_has_persons_id_persons_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_persons
    ADD CONSTRAINT movies_has_persons_id_persons_fkey FOREIGN KEY (id_persons) REFERENCES public.persons(id_persons);


--
-- Name: movies_has_persons movies_has_persons_id_persons_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies_has_persons
    ADD CONSTRAINT movies_has_persons_id_persons_type_fkey FOREIGN KEY (id_persons_type) REFERENCES public.persons_type(id_persons_type);


--
-- Name: persons persons_id_genders_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT persons_id_genders_fkey FOREIGN KEY (id_genders) REFERENCES public.genders(id_genders);


--
-- Name: ratings ratings_id_movies_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_id_movies_fkey FOREIGN KEY (id_movies) REFERENCES public.movies(id_movies);


--
-- Name: ratings ratings_id_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_id_users_fkey FOREIGN KEY (id_users) REFERENCES public.users(id_users);


--
-- Name: users_credentials users_credentials_id_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_credentials
    ADD CONSTRAINT users_credentials_id_users_fkey FOREIGN KEY (id_users) REFERENCES public.users(id_users);


--
-- Name: users users_id_genders_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_genders_fkey FOREIGN KEY (id_genders) REFERENCES public.genders(id_genders);


--
-- Name: users users_id_roles_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_roles_type_fkey FOREIGN KEY (id_roles_type) REFERENCES public.roles_type(id_roles_type);

