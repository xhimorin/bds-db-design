# _1. Project assignments_
Tento projekt je zamerany na navrah databazoveho systemu pre aplikaciu, ktoru si sami zvolime. 

## Obsah repozitaru
V repozitari sa nachadzaju priecinky, ktore obsahuju DDL skripty k importovaniu do databazoveho clienta (pgAdmin, phpMyAdmin).
Pre pgAdmin sa nachadzaju dva .sql files (1. pre implementaciu db, 2. pre inicializaciu dat).
Pre phpMyAdmin sa nachadza jeden .sql file v ktorom sa nachadza skript pre implementaciu a zaroven aj inicializaciu dat.

Nachadzaju sa tu aj screenshoty 
- `ERD_database_design.png` obsahuje narvh databazoveho systemu
- `pgAdmin.png` obsahuje screenshot z postgres clienta, sluzi ako ukazka, ze sa podarilo naimportovat dane DDL skripty 
- `phpMyAdmin.png` to iste ako v `pgAdmin.png` len pre clienta mysql

Dalej sa tu nachadza dokumentacia k projektu, ktora obsahuje uvod (obkec o danej problematike), vysvetlenie preco si myslim, ze som pouzil pri navrahu 3NF a nakoniec kratky opis tabuliek, ktore su v db systeme.
