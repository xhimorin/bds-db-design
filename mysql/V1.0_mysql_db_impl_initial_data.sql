-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 30, 2021 at 11:56 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imdb_movies`
--

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id_awards` int(10) UNSIGNED NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `awards`
--

INSERT IGNORE INTO `awards` (`id_awards`, `title`, `description`) VALUES
(1, 'Oscar', 'Za najlepší film.'),
(2, 'Oscar', 'Za najlepší filmovou hudbu.'),
(3, 'Oscar', 'Za najlepší kameru.'),
(4, 'Oscar', 'Za najlepší střih zvuku.'),
(5, 'MTV Movie Award', 'Za najlepší souboj.');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id_genders` int(10) UNSIGNED NOT NULL,
  `gender_type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genders`
--

INSERT IGNORE INTO `genders` (`id_genders`, `gender_type`) VALUES
(1, 'muž'),
(2, 'žena'),
(3, 'x-men');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id_genres` int(10) UNSIGNED NOT NULL,
  `genre_type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genres`
--

INSERT IGNORE INTO `genres` (`id_genres`, `genre_type`) VALUES
(1, 'Sci-Fi'),
(2, 'Akční'),
(3, 'Dobrodružný'),
(4, 'Drama'),
(5, 'Krimi'),
(6, 'Thriller'),
(7, 'Komedie'),
(8, 'Rodinný'),
(9, 'Romantický'),
(10, 'Mysteriózní'),
(11, 'Psychologický'),
(12, 'Horor'),
(13, 'Sportovní'),
(14, 'Životopisný');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id_interests` int(10) UNSIGNED NOT NULL,
  `interest` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `interests`
--

INSERT IGNORE INTO `interests` (`id_interests`, `interest`) VALUES
(1, 'Režisér Denis Villeneuve preferoval při natáčení využívání praktických efektů, proto natáčení probíhalo převážně v exteriérech. „Čelisti se také nenatáčely v bazénu,“ vysvětlil rozhodnutí Villeneuve.'),
(2, 'Závěrečná scéna byla ve skutečnosti natočena na Panenských ostrovech v Karibiku, avšak ve filmu se má odehrávat na pobřeží Tichého oceánu.'),
(3, 'Hned při první scéně, kde má hlavní postava (Edward Norton) na parkovišti udeřit Tylera (Brad Pitt) \"silně, jak jen může\", Norton opravdu trefí Pitta do ucha, ač měl úder jen předstírat. To proto, že si režisér David Fincher před scénou vzal Nortona stranou a řekl mu, ať Pitta udeří doopravdy, aby záběr vypadal zcela reálně. Proto ve výsledné scéně můžeme vidět, jak je Pitt skutečně v bolestech a Norton se jen směje.');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(128) NOT NULL,
  `publication_year` int(11) NOT NULL,
  `country` varchar(32) NOT NULL,
  `url_trailer` varchar(256) DEFAULT NULL,
  `contents` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movies`
--

INSERT IGNORE INTO `movies` (`id_movies`, `title`, `publication_year`, `country`, `url_trailer`, `contents`) VALUES
(1, 'Duna', 2021, 'USA', 'https://www.youtube.com/watch?v=8g18jFHCLXk', 'Kultovní sci-fi dílo vypráví o mocenských bojích uvnitř galaktického Impéria, v nichž jde o ovládnutí planety Arrakis: zdroje vzácného koření - melanže, jež poskytuje zvláštní psychické schopnosti, které umožňují cestování vesmírem. Padišáh imperátor svěří správu nad Arrakisem a s ní i komplikovanou těžbu neobyčejné látky vévodovi rodu Atreidů. Celá anabáze je ale součástí spiknutí, z něhož se podaří vyváznout jen vévodovu synovi Paulovi a jeho matce, kteří uprchnou do pouště. Jejich jedinou nadějí jsou nedůvěřiví domorodí obyvatelé fremeni, schopní trvale přežít ve vyprahlé pustině. Mohl by Paul Atreides být spasitelem, na kterého tak dlouho čekají?'),
(2, 'Vykoupení z věznice Shawshank', 1994, 'USA', 'https://www.youtube.com/watch?v=iorHgfCOhK8', 'Mladý bankovní manažer Andy Dufresne (Tim Robbins) je v roce 1947 odsouzen na doživotí za vraždu své ženy a jejího milence, kterou nespáchal. Čeká ho trest v obávané věznici Shawshank. Andy se zde sblíží s černochem Redem (Morgan Freeman), jenž je tu už dvacet let, a během dlouhé doby se jim společně podaří dosáhnout zlepšení zdejších poměrů. Andy se dokonce stane strážcům i nenáviděnému řediteli věznice nepostradatelný jako daňový a finanční poradce.'),
(3, 'Klub rváčů', 1999, 'USA', 'https://www.youtube.com/watch?v=MAidmBZD0Bk', 'Když nemůžete půl roku usnout, celý okolní svět vám začne připadat jako nekonečný sen. Všechno kolem vás je nedokonalou xeroxovou kopií sebe sama. Chodíte do práce, díváte se na televizi a jste vděčni za to, když občas ztratíte vědomí a nevíte o světě. Lidí s podobnými problémy moc není, ale mladý úspěšný úředník, který si říká Jack, je jedním z nich. Má slušnou práci, vydělává slušné peníze, ale trpí nejtěžší formou nespavosti. Na služební cestě se Jack seznámí s Tylerem Durdenem, který mu nabídne příbytek pod podmínkou, že mu vrazí pořádnou ránu. Tato \"výměna názorů\" se oběma zalíbí a brzy vznikne první Klub rváčů. Místo, kde můžou mladí muži, znechucení světem, odložit své starosti a stát se na pár minut zvířaty.'),
(4, 'Hon', 2012, 'Dánsko', 'https://www.youtube.com/watch?v=MxQi3KUXc94', NULL),
(5, 'Na samotě u lesa', 1976, 'Československo', NULL, 'Úsměvný příběh, ve kterém možná poznáte sami sebe. Pátečním odpolednem vyjíždí Oldřich a Věra Lavičkovi se svými dětmi poprvé na chalupu. Sice žádnou nemají, ale touží po ní a doufají, že přátelé v Loukově jim již nějakou vyhlédli. A opravdu. Jenomže v ní bydlí děda Komárek. Jak to bývalo zvykem, slíbí mu, že ho nechají v chalupě dožít, ale soukromí by přece jenom bylo lepší. Jedině děti přilnou k dědovi, jako by byl jejich vlastní, a jdou tak rodičům příkladem. Když děda onemocní, Lavičkovi si uvědomí, že nemají žádný doklad o tom, že jim chalupa bude po dědově smrti patřit, a bleskově se vypraví i s notářem do Loukova...'),
(6, 'Nedotknutelní', 2011, 'Francie', 'https://www.youtube.com/watch?v=d1hTDI4lt0k', 'Ochrnutý a bohatý aristokrat Philippe si za svého nového opatrovníka vybere Drisse, živelného mladíka z předměstí, kterého právě propustili z vězení. Jinými slovy - najde si na tuto práci tu nejméně vhodnou osobu. Podaří se jim však propojit nemožné: Vivaldiho a populární hudbu, serióznost a žoviální vtípky, luxusní obleky a tepláky. Bláznivým, zábavným, silným, neočekávaným a hlavně „nedotknutelným“, přesně takovým se stane jejich přátelství… Komedie s dramatickou zápletkou o tom, že ani od krku po prsty u nohou nepohyblivý člověk odkázaný na pomoc druhých, nemusí ztratit smysl života. A o tom, že i nejméně pravděpodobné spojení melancholického multimilionáře a extrovertního recidivisty může humorně zapůsobit na diváka a může se z něj stát kasovní trhák.'),
(7, 'Pulp Fiction: Historky z podsvětí', 1994, 'USA', 'https://www.youtube.com/watch?v=zt1uSDXL5NA', 'Scenárista, režisér a herec Quentin Tarantino (nar. 1963) je bezesporu jedním z nejvýznamnějších tvůrců současného světového filmu. Zaujal už svojí celovečerní prvotinou Gauneři (1991) a svým druhým filmem Pulp Fiction – historky z podsvětí (1994) jen potvrdil své dominantní postavení mezi postmoderními režiséry. Scénář filmu vznikl důmyslným propojením tří povídek spjatých se světem zločinu, jež jsou vyprávěny bez ohledu na časovou posloupnost a nakonec vytvoří uzavřený kruh. Původní název filmu, odkazující k pokleslé krvákové literatuře, dokonale souzní s tím, co se děje na plátně. Výbuchy brutálního násilí jsou odlehčeny velmi černým humorem a jedna bizarní situace za druhou ústí do ještě bizarnějších konců. Kromě novátorského scénáře a jisté režie film zaujme i přehlídkou mimořádných hereckých výkonů přesně obsazených představitelů, mj. Johna Travolty a Samuela L. Jacksona (zabijáci Vincent a Jules), Bruce Willise (unavený boxer Butch), Tima Rotha a Amandy Plummerové (milenecká dvojice, chystající se k přepadení) či Umy Thurmanové (gangsterská manželka Mia). Snímek byl uveden v soutěži MFF v Cannes 1994, odkud si odnesl nejvyšší ocenění, Zlatou palmu. Členové Americké akademie ho nominovali na Oscara v sedmi kategoriích (mj. i za nejlepší film a režii), ale zlatou sošku nakonec dostal pouze scénář filmu. V témže roce dominoval v rámci nezávislé tvorby a získal celkem čtyři Independent Spirit Awards (Ceny nezávislého ducha), a sice za nejlepší film, režii, scénář a herecký výkon v hlavní mužské roli (Samuel L. Jackson).'),
(8, 'Gran Torino', 2008, 'Německo', 'https://www.youtube.com/watch?v=RMhbr2XQblk', NULL),
(9, 'Zelená míle', 1999, 'USA', 'https://www.youtube.com/watch?v=Ki4haFrqSrw', 'Brilantní filmové drama Zelená míle podle knižní předlohy Stephena Kinga natočil režisér Frank Darabont, který se zároveň podílel i na jeho scénáři. Film s vynikajícím Tomem Hanksem v hlavní roli, byl nominován na čtyři Oscary. Centrem příběhu je černý obr John Coffey (Michael Clarke Duncan), čekající na popravu za zločin, který dost možná nespáchal a přesto zemře, a dále pak věčné dilema kolem rozhodování o trestu smrti. Děj filmu se odehrává ve věznici, kde sedí ti největší hříšníci, a kde je hrůzostrašná chodba natřená na zeleno, které všichni říkají Zelená míle. Chodba, na jejímž konci čeká smrt. Pokud se nestane zázrak...'),
(10, 'Forrest Gump', 1994, 'USA', 'https://www.youtube.com/watch?v=bLvqoHBptjg', NULL),
(11, 'Kmotr', 1972, 'USA', NULL, 'Doposud nevinný syn mafiánského bosse je vtažen do krvavého rodinného byznysu, když je jeho otec, Don Vito Corleone, hlava newyorské mafiánské rodiny vážně zraněn. Don Vito Corleone je hlava newyorské mafiánské „rodiny“. Když gangster Sollozzo, který má za zády jinou rodinu mafiánů, oznámí, že chce po celém New Yorku prodávat drogy, přichází problém. Don Vito se důrazně staví proti obchodu s drogami a docela mu stačí hazard, vybírání výpalného a podobné aktivity, kterými si vydělává. Dojde tedy k pokusu o jeho vraždu. Sollozzo pak unese jednoho z poradců Dona Vita a pokusí se přinutit syna Dona Vita, aby s prodejem drog souhlasil, plán ovšem zkrachuje, když Sollozzo zjistí, že je Don Vito stále naživu'),
(12, 'Mlčení jehňátek', 1991, 'USA', 'https://www.youtube.com/watch?v=lu-pBQ7kNb4', 'Mladá citlivá agentka FBI Clarice Steriling je přizvána ke spolupráci do týmu vyšetřujícího sérii vražd. Na pachatele ukazují jen velmi kruté následky jeho činů: Vrah přezdívaný \"Buffalo Bill\" stahuje vždycky své oběti z kůže a potom je hází do řeky. Clarice má za úkol vypracovat psychologický portrét pachatele. Její nadřízení ji proto vyšlou na \"konzultaci\" za jiným masovým vrahem, psychiatrem dr. Hannibalem Lecterem, který je vězněn za nejvyšších bezpečnostních opatření. Lecter, který zná ze své praxe vraha i jeho motivy a tuší jeho další postup, drží v rukou všechny nitky a přivádí mladou Clarice na správnou stopu...'),
(13, 'Sedm', 1995, 'USA', 'https://www.youtube.com/watch?v=znmZoVkCjpI', NULL),
(14, 'The Matrix', 1999, 'USA', 'https://www.youtube.com/watch?v=vKQi3bBA1y8', NULL),
(15, 'Vetřelec', 1979, 'Velká Británie', NULL, 'Vesmírná loď Nostromo míří zpět na Zemi, astronauti spí. Náhle dojde k jejich automatickému probuzení – nikdo neví, co se děje, protože jsou ještě velmi daleko od cíle cesty. Prostřednictvím centrálního počítače zjistí, že přijímače zachytily signál SOS z blízké planety. Posádka je povinna věc vyšetřit. Vyslaný modul má s přistáním velké problémy. Přesto někteří na průzkum. V jeskynním komplexu objeví podivný organizmus, který vypadá jako kolonie vajíček. Počítač mateřské lodi mezitím dešifruje zprávu jako varování, ne jako volání o pomoc. Všichni jsou okamžitě odvoláni. Jenže jedno z vajíček se otevře a jakási hmota ve tvaru chobotnice se během setiny sekundy přisaje na přilbu jednoho z astronautů, přesněji řečeno na jeho obličej. Přes odpor Ripleyové, že je proti bezpečnostním předpisům brát na palubu cokoli neznámého, otevře Ash dveře a všechny vpustí. Po nějaké době je nalezen cizí organismus na podlaze, sám odpadl. Vypadá to, že se postižený těší dobrému zdraví. Při společném jídle však stolující zažijí hrůzné představení. Před jejich očima vyleze z astronautova břicha nějaký tvor, a než stačí kdokoli cokoli podniknout, bleskovou rychlostí zmizí v obrovských prostorách lodi. Začíná noční můra – monstrum postupně likviduje posádku a zdá se, že je nezničitelné… '),
(16, 'Rivalové', 2013, 'Německo', 'https://www.youtube.com/watch?v=6k0QyHapx0w', NULL),
(17, 'Počátek', 2010, 'Velká Británie', 'https://www.youtube.com/watch?v=6XJ4JQFFYO8', 'Dom Cobb (Leonardo DiCaprio) je velmi zkušený zloděj a jeho největší mistrovství je v krádeži nejcennějších tajemství. Ovšem není to jen tak obyčejný zloděj. Dom krade myšlenky z lidského podvědomí v době, kdy lidská mysl je nejzranitelnější – když člověk spí. Cobbova nevšední dovednost z něj dělá nejen velmi vyhledávaného experta, ale také ohroženého uprchlíka. Musel obětovat vše, co kdy miloval. Nyní se mu však nabízí šance na vykoupení. Může získat zpět svůj život. Tato poslední zakázka je nejen velmi riskantní, ale zdá se, že i nemožná. Tentokrát nemá za úkol myšlenku ukrást, ale naopak ji zasadit do něčí mysli. Pokud uspěje, bude to dokonalý zločin.');

-- --------------------------------------------------------

--
-- Table structure for table `movies_has_awards`
--

CREATE TABLE `movies_has_awards` (
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `id_awards` int(10) UNSIGNED NOT NULL,
  `award_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movies_has_awards`
--

INSERT IGNORE INTO `movies_has_awards` (`id_movies`, `id_awards`, `award_year`) VALUES
(2, 1, 1995),
(2, 2, 1995),
(2, 3, 1995),
(3, 4, 2000),
(3, 5, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `movies_has_genres`
--

CREATE TABLE `movies_has_genres` (
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `id_genres` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movies_has_genres`
--

INSERT IGNORE INTO `movies_has_genres` (`id_movies`, `id_genres`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 4),
(2, 5),
(3, 4),
(3, 6),
(4, 4),
(5, 7),
(5, 8),
(5, 4),
(6, 7),
(6, 14),
(6, 4),
(7, 4),
(7, 5),
(8, 4),
(9, 4),
(9, 10),
(9, 5),
(10, 7),
(10, 4),
(10, 9),
(11, 4),
(11, 5),
(12, 6),
(12, 5),
(12, 10),
(12, 11),
(12, 4),
(13, 5),
(13, 6),
(13, 12),
(13, 4),
(14, 1),
(14, 2),
(15, 1),
(15, 12),
(16, 2),
(16, 4),
(16, 13),
(16, 14),
(17, 1),
(17, 2),
(17, 3),
(17, 6),
(17, 10);

-- --------------------------------------------------------

--
-- Table structure for table `movies_has_interests`
--

CREATE TABLE `movies_has_interests` (
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `id_interests` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movies_has_interests`
--

INSERT IGNORE INTO `movies_has_interests` (`id_movies`, `id_interests`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `movies_has_persons`
--

CREATE TABLE `movies_has_persons` (
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `id_persons` bigint(20) UNSIGNED NOT NULL,
  `id_persons_type` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movies_has_persons`
--

INSERT IGNORE INTO `movies_has_persons` (`id_movies`, `id_persons`, `id_persons_type`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 2),
(2, 4, 1),
(2, 5, 2),
(2, 6, 2),
(3, 7, 1),
(3, 8, 2),
(3, 9, 2),
(4, 10, 1),
(4, 11, 2),
(4, 12, 2),
(5, 13, 1),
(5, 14, 2),
(5, 15, 2),
(6, 16, 1),
(6, 17, 2),
(6, 18, 2),
(7, 19, 1),
(7, 20, 2),
(7, 21, 2),
(8, 22, 1),
(8, 23, 2),
(8, 24, 2),
(9, 25, 1),
(9, 26, 2),
(9, 27, 2),
(10, 28, 1),
(10, 26, 2),
(10, 29, 2),
(11, 31, 1),
(11, 32, 2),
(11, 33, 2),
(12, 34, 1),
(12, 35, 2),
(12, 36, 2),
(13, 6, 1),
(13, 5, 2),
(13, 8, 2),
(14, 37, 1),
(14, 38, 2),
(14, 39, 2),
(15, 40, 1),
(15, 41, 2),
(15, 42, 2),
(16, 43, 2),
(17, 44, 1),
(17, 45, 2),
(17, 46, 2);

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id_persons` bigint(20) UNSIGNED NOT NULL,
  `id_genders` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `persons`
--

INSERT IGNORE INTO `persons` (`id_persons`, `id_genders`, `first_name`, `surname`, `birthdate`, `country`) VALUES
(1, 1, 'Denis', 'Villeneuve', '1967-10-03', 'Kanada'),
(2, 2, 'Rebecca', 'Ferguson', '1983-10-19', NULL),
(3, 1, 'Timothée', 'Chalamet', '1995-12-27', NULL),
(4, 1, 'Frank', 'Darabont', '1959-01-28', 'Francie'),
(5, 1, 'Morgan', 'Freeman', '1937-06-01', 'USA'),
(6, 1, 'David', 'Fincher', '1962-08-28', 'USA'),
(7, 1, 'Edward', 'Norton', '1969-08-18', NULL),
(8, 1, 'Brad', 'Bitt', NULL, NULL),
(9, 1, 'Thomas', 'Vinterberg', '1969-05-19', 'Dánsko'),
(10, 1, 'Mads', 'Mikkelsen', '1965-11-22', 'Dánsko'),
(11, 2, 'Annika', 'Wedderkopp', NULL, NULL),
(12, 1, 'Jiří', 'Menzel', '1938-02-05', 'Česko'),
(13, 1, 'Josef', 'Kemr', '1922-06-15', 'Česko'),
(14, 2, 'Naďa', 'Urbánková', NULL, NULL),
(15, 1, 'Eric', 'Toledano', '1971-07-03', 'Francie'),
(16, 1, 'Omar', 'Sy', '1978-01-20', 'Francie'),
(17, 2, 'Joséphine', 'Meaux', '1977-01-23', 'Francie'),
(18, 1, 'Quentin', 'Tarantino', '1963-03-27', 'USA'),
(19, 1, 'John', 'Travolta', '1954-02-18', 'USA'),
(20, 1, 'Samuel', 'Jackson', '1948-12-21', 'USA'),
(21, 1, 'Clint', 'Eastwood', '1930-05-31', 'USA'),
(22, 2, 'Dreama', 'Walker', '1986-06-20', 'USA'),
(23, 2, 'Ahney', 'Her', '1992-07-13', 'USA'),
(24, 1, 'Tom', 'Hanks', '1956-07-09', 'USA'),
(25, 1, 'David', 'Morse', '1953-10-11', 'USA'),
(26, 1, 'Robert', 'Zemeckis', '1951-05-14', 'USA'),
(27, 2, 'Robin', 'Wright', '1966-04-08', 'USA'),
(28, 1, 'Francis', 'Coppola', '1939-04-07', 'USA'),
(29, 1, 'Marlon', 'Brando', '1924-04-03', 'USA'),
(30, 1, 'Al', 'Capone', '1940-04-25', 'USA'),
(31, 1, 'Jonathan', 'Demme', '1944-02-22', 'USA'),
(32, 2, 'Jadie', 'Foster', '1962-11-19', 'USA'),
(33, 1, 'Anthony ', 'Hopkins', '1937-12-31', 'USA'),
(34, 2, 'Lilly', 'Wachowski', '1967-12-29', NULL),
(35, 1, 'Keanu', 'Reeves', '1964-09-02', 'Libanon'),
(36, 1, 'Laurence', 'Fishburne', '1961-07-30', 'USA'),
(37, 1, 'Ridley', 'Scott', '1937-11-30', 'Velká Británie'),
(38, 2, 'Sigourney', 'Weaver', NULL, NULL),
(39, 1, 'Tom', 'Skerritt', '1933-08-25', 'USA'),
(40, 2, 'Veronica', 'Cartwright', '1949-04-20', 'Velká Británie'),
(41, 1, 'Ron', 'Howard', '1954-03-01', 'USA'),
(42, 1, 'Daniel', 'Brühl', '1978-06-16', 'Španělsko'),
(43, 1, 'Chris', 'Hemsworth', '1983-08-11', 'Austrálie'),
(44, 1, 'Christopher', 'Nolan', '1970-07-30', 'Velká Británie'),
(45, 1, 'Leonardo', 'DiCaprio', '1974-11-11', 'USA'),
(46, 3, 'Elliot', 'Page', '1987-02-21', 'Kanada');

-- --------------------------------------------------------

--
-- Table structure for table `persons_type`
--

CREATE TABLE `persons_type` (
  `id_persons_type` int(10) UNSIGNED NOT NULL,
  `person_type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `persons_type`
--

INSERT IGNORE INTO `persons_type` (`id_persons_type`, `person_type`) VALUES
(1, 'Režisér'),
(2, 'Herec');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id_ratings` bigint(20) UNSIGNED NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `id_movies` bigint(20) UNSIGNED NOT NULL,
  `rating_movie` int(11) DEFAULT NULL,
  `comment_movie` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ratings`
--

INSERT IGNORE INTO `ratings` (`id_ratings`, `id_users`, `id_movies`, `rating_movie`, `comment_movie`) VALUES
(1, 2, 1, 4, 'Paradny film.'),
(2, 2, 5, 4, 'Paradny film.'),
(3, 3, 4, NULL, 'Radsej to nehodnotim.'),
(4, 4, 4, 3, 'Celkom dobry film.'),
(5, 4, 5, 5, 'Kraaaaaasa'),
(6, 4, 2, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles_type`
--

CREATE TABLE `roles_type` (
  `id_roles_type` int(10) UNSIGNED NOT NULL,
  `role_type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles_type`
--

INSERT IGNORE INTO `roles_type` (`id_roles_type`, `role_type`) VALUES
(1, 'admin'),
(2, 'užívateľ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `id_roles_type` int(10) UNSIGNED NOT NULL,
  `id_genders` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `mail` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT IGNORE INTO `users` (`id_users`, `id_roles_type`, `id_genders`, `first_name`, `surname`, `mail`, `username`, `birthdate`) VALUES
(1, 1, 3, 'Admin', 'Adminovský', 'admin@admin.ad', 'admin', '1998-12-24'),
(2, 2, 2, 'Maroš', 'German', 'grc@mail..', 'grg', '2000-01-01'),
(3, 2, 1, 'Roman', 'Klampár', 'rk@kr.rrr', 'xkl', '1978-09-09'),
(4, 2, 2, 'Ján', 'Špageta', 'jjj@sss.qq', 'recenzent', '2002-08-08'),
(5, 2, 1, 'John', 'Doe', 'john@doe.cz', 'johan', '1970-02-12');

-- --------------------------------------------------------

--
-- Table structure for table `users_credentials`
--

CREATE TABLE `users_credentials` (
  `id_users_credentials` bigint(20) UNSIGNED NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `user_password` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_credentials`
--

INSERT IGNORE INTO `users_credentials` (`id_users_credentials`, `id_users`, `user_password`) VALUES
(1, 1, 'admin'),
(2, 2, '574&Hhca2'),
(3, 3, '@#&!%sss9'),
(4, 4, 'heslo'),
(5, 5, 'toto_nieje@heslo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id_awards`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id_genders`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id_genres`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id_interests`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id_movies`);

--
-- Indexes for table `movies_has_awards`
--
ALTER TABLE `movies_has_awards`
  ADD KEY `id_movies` (`id_movies`),
  ADD KEY `id_awards` (`id_awards`);

--
-- Indexes for table `movies_has_genres`
--
ALTER TABLE `movies_has_genres`
  ADD KEY `id_movies` (`id_movies`),
  ADD KEY `id_genres` (`id_genres`);

--
-- Indexes for table `movies_has_interests`
--
ALTER TABLE `movies_has_interests`
  ADD KEY `id_movies` (`id_movies`),
  ADD KEY `id_interests` (`id_interests`);

--
-- Indexes for table `movies_has_persons`
--
ALTER TABLE `movies_has_persons`
  ADD KEY `id_movies` (`id_movies`),
  ADD KEY `id_persons` (`id_persons`),
  ADD KEY `id_persons_type` (`id_persons_type`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id_persons`),
  ADD KEY `id_genders` (`id_genders`);

--
-- Indexes for table `persons_type`
--
ALTER TABLE `persons_type`
  ADD PRIMARY KEY (`id_persons_type`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id_ratings`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_movies` (`id_movies`);

--
-- Indexes for table `roles_type`
--
ALTER TABLE `roles_type`
  ADD PRIMARY KEY (`id_roles_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD KEY `id_roles_type` (`id_roles_type`),
  ADD KEY `id_genders` (`id_genders`);

--
-- Indexes for table `users_credentials`
--
ALTER TABLE `users_credentials`
  ADD PRIMARY KEY (`id_users_credentials`),
  ADD KEY `id_users` (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id_awards` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id_genders` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id_genres` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id_interests` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id_movies` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id_persons` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `persons_type`
--
ALTER TABLE `persons_type`
  MODIFY `id_persons_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id_ratings` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles_type`
--
ALTER TABLE `roles_type`
  MODIFY `id_roles_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_credentials`
--
ALTER TABLE `users_credentials`
  MODIFY `id_users_credentials` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `movies_has_awards`
--
ALTER TABLE `movies_has_awards`
  ADD CONSTRAINT `movies_has_awards_ibfk_1` FOREIGN KEY (`id_movies`) REFERENCES `movies` (`id_movies`),
  ADD CONSTRAINT `movies_has_awards_ibfk_2` FOREIGN KEY (`id_awards`) REFERENCES `awards` (`id_awards`);

--
-- Constraints for table `movies_has_genres`
--
ALTER TABLE `movies_has_genres`
  ADD CONSTRAINT `movies_has_genres_ibfk_1` FOREIGN KEY (`id_movies`) REFERENCES `movies` (`id_movies`),
  ADD CONSTRAINT `movies_has_genres_ibfk_2` FOREIGN KEY (`id_genres`) REFERENCES `genres` (`id_genres`);

--
-- Constraints for table `movies_has_interests`
--
ALTER TABLE `movies_has_interests`
  ADD CONSTRAINT `movies_has_interests_ibfk_1` FOREIGN KEY (`id_movies`) REFERENCES `movies` (`id_movies`),
  ADD CONSTRAINT `movies_has_interests_ibfk_2` FOREIGN KEY (`id_interests`) REFERENCES `interests` (`id_interests`);

--
-- Constraints for table `movies_has_persons`
--
ALTER TABLE `movies_has_persons`
  ADD CONSTRAINT `movies_has_persons_ibfk_1` FOREIGN KEY (`id_movies`) REFERENCES `movies` (`id_movies`),
  ADD CONSTRAINT `movies_has_persons_ibfk_2` FOREIGN KEY (`id_persons`) REFERENCES `persons` (`id_persons`),
  ADD CONSTRAINT `movies_has_persons_ibfk_3` FOREIGN KEY (`id_persons_type`) REFERENCES `persons_type` (`id_persons_type`);

--
-- Constraints for table `persons`
--
ALTER TABLE `persons`
  ADD CONSTRAINT `persons_ibfk_1` FOREIGN KEY (`id_genders`) REFERENCES `genders` (`id_genders`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`),
  ADD CONSTRAINT `ratings_ibfk_2` FOREIGN KEY (`id_movies`) REFERENCES `movies` (`id_movies`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_roles_type`) REFERENCES `roles_type` (`id_roles_type`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_genders`) REFERENCES `genders` (`id_genders`);

--
-- Constraints for table `users_credentials`
--
ALTER TABLE `users_credentials`
  ADD CONSTRAINT `users_credentials_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
